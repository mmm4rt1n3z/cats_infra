locals {
  name = "funny-cats"
}
module "vpc" {
  source = "../../modules/vpc"
}
module "security_group" {
  source = "../../modules/security_group"
  from_port = "8000"
  to_port = "8000"
  vpc_id = module.vpc.vpc_id
  name = local.name
  vpc_cidr = module.vpc.vpc_cidr
}

module "alb" {
  source = "../../modules/alb"
  depends_on = [module.vpc]

  name = local.name
  public_subnets_ids = [module.vpc.public_subnet_id, module.vpc.public_subnet_id1]
  vpc_id = module.vpc.vpc_id
}

module "alb_target_group" {
  source = "../../modules/alb_target_group"
  depends_on = [module.alb, module.vpc]

  name = local.name
  vpc_id = module.vpc.vpc_id
}

module "alb_listener_https" {
  source = "../../modules/alb_listener"
  depends_on = [module.alb, module.alb_target_group]
  alb_arn = module.alb.arn
  target_group_id = module.alb_target_group.id
}


module "ecr" {
  source = "../../modules/ecr"
  account_id = "your_account_id"
  image = "funny-cats"
  region = "us-west-1"
  name = local.name
}



module "funny_cats_ecs" {
  source = "../../modules/ecs"
  depends_on = [module.alb_target_group, module.vpc, module.security_group, module.alb, module.alb_listener_https]

  alb_target_group_arn = module.alb_target_group.arn
  cluster_name = local.name
  container_image = module.ecr.repository_url
  container_name = local.name
  container_port = 8000
  region = "us-west-1"
  security_groups = [module.security_group.id]
  service_name = local.name
  subnets_ids = [module.vpc.public_subnet_id]
  task_cpu = 512
  task_memory = 1024
}

