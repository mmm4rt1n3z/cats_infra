output "arn" {
  value = aws_lb_target_group.main.arn
}

output "id" {
  value = aws_lb_target_group.main.id
}

output "name" {
  value = aws_lb_target_group.main.name
}