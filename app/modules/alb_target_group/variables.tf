variable "name" {
  type = string
}

variable "vpc_id" {
  type = string
}

variable "port_number" {
  type = number
  default = 80
}

variable "deregistration_delay" {
  type = number
  description = "Number of seconds that the target group will wait to change the status of the target"
  default = 20
}

variable "healthy_threshold" {
  type = string
  default = "2"
  description = "The string representation of the number of healthy checks needed to mark a target as healthy"
}

variable "unhealthy_threshold" {
  type = string
  default = "3"
  description = "The string representation of the number of unhealthy checks needed to mark a target as unhealthy"
}

variable "matcher" {
  type = string
  default = "200,301"
  description = "The status response accepted by the target group as a healthy response (301 is in case the backend force ssl)"
}

variable "timeout" {
  type = string
  default = "10"
  description = "Timeout for a health check"
}

variable "path" {
  type = string
  default = "/"
  description = "The path where the target group will perform the request"
}

variable "interval" {
  type = string
  default = "20"
  description = "Approximate amount of time, in seconds, between health checks of an individual target"
}

variable "protocol" {
  type = string
  description = "The protocol to be used by the Target group"
  default = "HTTP"
}

variable "environment" {
  type = string
  default = "<%= "#{Terraspace.env}" %>"
}

variable "tags" {
  type = map(string)
  default = {}
}
