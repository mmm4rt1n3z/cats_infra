resource "aws_lb_target_group" "main" {
  name     = var.name
  port     = var.port_number
  protocol = var.protocol
  target_type = "ip"
  vpc_id   = var.vpc_id
  deregistration_delay = var.deregistration_delay

  health_check {
    healthy_threshold   = var.healthy_threshold
    interval            = var.interval
    protocol            = var.protocol
    matcher             = var.matcher
    timeout             = var.timeout
    path                = var.path
    unhealthy_threshold = var.unhealthy_threshold
  }

  tags = merge(
  {
    Name        = "${var.name}-${var.environment}",
    environment = var.environment,
  },
  var.tags
  )
}
