locals {
  name = "${var.name}-${var.environment}"
}

resource "aws_ecr_repository" "main" {
  name = local.name

  image_scanning_configuration {
    scan_on_push = var.scan_on_push
  }

  provisioner "local-exec" {
    command = "aws ecr get-login-password --region ${var.region} --profile ${var.environment} | docker login --username AWS --password-stdin ${var.account_id}.dkr.ecr.${var.region}.amazonaws.com/"
    on_failure = continue
  }
  provisioner "local-exec" {
    command = "docker tag ${var.image} ${var.account_id}.dkr.ecr.${var.region}.amazonaws.com/${var.name}-${var.environment}:latest"
  }
  provisioner "local-exec" {
    command = "docker push ${var.account_id}.dkr.ecr.${var.region}.amazonaws.com/${var.name}-${var.environment}:latest"
  }

  tags = merge(
  {
    Name = local.name,
    environment = var.environment
  },
  var.tags,
  )
}
