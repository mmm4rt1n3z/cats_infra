variable "name" {
  type = string
  description = "The name of the images repository"
}

variable "environment" {
  description = "The environment for this repository"
  type = string
  default = "<%= "#{Terraspace.env}" %>"
}

variable "region" {
  type = string
}

variable "account_id" {
  type = string
}

variable "image" {
  type = string
}

variable "tags" {
  type = map(string)
  default = {}
}
variable "scan_on_push" {
  type = bool
  default = false
  description = "Whether the image needs to be scanned after a push or not"
}



