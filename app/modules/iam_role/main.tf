resource "aws_iam_role" "main" {
  name = var.name
  assume_role_policy = var.policy_to_assume
  tags = var.tags
}