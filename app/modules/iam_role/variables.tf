variable "name" {
  type = string
}

variable "policy_to_assume" {
  type = string
  description = "A JSON representation of the policy to assume"
}

variable "tags" {
  type = map(string)
  default = {}
}