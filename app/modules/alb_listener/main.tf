#redirect alb
resource "aws_lb_listener" "main" {
  load_balancer_arn = var.alb_arn
  port              = var.port
  protocol          = var.protocol


  default_action {
    target_group_arn = var.target_group_id
    type             = "forward"
  }
}
