output "arn" {
  value = aws_lb_listener.main.arn
}

output "id" {
  value = aws_lb_listener.main.id
}