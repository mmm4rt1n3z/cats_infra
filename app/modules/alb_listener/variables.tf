variable "alb_arn" {
  type = string
}

variable "port" {
  type = string
  default = "80"
}

variable "protocol" {
  type = string
  default = "HTTP"
}

variable "target_group_id" {
  type = string
}
