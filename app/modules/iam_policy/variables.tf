variable "name" {
  type = string
}

variable "policy" {
  type = string
  description = "A JSON representation of the policy"
}

variable "description" {
  type = string
  default = "You should provide descriptions for your policies..."
}