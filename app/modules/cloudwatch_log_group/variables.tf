variable "name" {
  type = string
}

variable "environment" {
  type = string
  default = "<%= "#{Terraspace.env}" %>"
}

variable "tags" {
  type = map(string)
  default = {}
}

variable "retention_days" {
  type = number
  default = 5
  description = "The number of days that a set of logs will remain available"
}