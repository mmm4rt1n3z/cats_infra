resource "aws_cloudwatch_log_group" "main" {
  name = var.name

  retention_in_days = var.retention_days

  tags = merge(
  {
    Name = var.name,
    environment = var.environment
  },
  var.tags,
  )
}
