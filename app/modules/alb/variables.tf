variable "name" {
  type = string
}

variable "environment" {
  description = "The environment for this repository"
  type = string
  default = "<%= "#{Terraspace.env}" %>"
}

variable "tags" {
  type = map(string)
  default = {}
}

variable "vpc_id" {
  type = string
}

variable "public_subnets_ids" {
  type = list(string)
}

variable "internal" {
  type = bool
  default = false
}

variable "enable_deletion_protection" {
  type = bool
  default = false
}


variable "type" {
  type = string
  default = "application"
  description = "The type of ALB it accepts: application, network, gateway"
}

variable "from_port" {
  type = number
  default = 80
}

variable "to_port" {
  type = number
  default = 80
}

variable "vpc_cidr" {
  type = string
  default = "0.0.0.0/0"
}
