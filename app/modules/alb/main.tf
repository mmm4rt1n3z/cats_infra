
module "alb_sg" {
  source = "../security_group"
  name = "${var.name}-${var.environment}"
  from_port = var.from_port
  to_port = var.to_port
  vpc_cidr = var.vpc_cidr
  vpc_id = var.vpc_id
}

resource "aws_lb" "main" {
  name               = "${var.name}-${var.environment}"
  internal           = var.internal
  load_balancer_type = var.type
  security_groups    = [module.alb_sg.id]
  subnets            = var.public_subnets_ids

  enable_deletion_protection = var.enable_deletion_protection

  tags = merge(
  {
    Name        = "${var.name}-${var.environment}",
    environment = var.environment,
  },
  var.tags
  )
}
