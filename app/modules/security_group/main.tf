resource "aws_security_group" "main" {
  name        = "${var.name}-${var.environment}"
  vpc_id      = var.vpc_id

  #accessible from TLS (any ip)
  ingress {
      from_port        = 443
      to_port          = 443
      protocol         = "tcp"
      cidr_blocks      = [var.vpc_cidr]
      ipv6_cidr_blocks = ["::/0"]
    }
  ingress {
      from_port        = var.from_port
      to_port          = var.to_port
      protocol         = "tcp"
      cidr_blocks      = [var.vpc_cidr]
      ipv6_cidr_blocks = ["::/0"]
    }

  egress {
      from_port        = 0
      to_port          = 0
      protocol         = "-1"
      cidr_blocks      = ["0.0.0.0/0"]
      ipv6_cidr_blocks = ["::/0"]
    }

  tags = merge(
  {
    environment = var.environment,
    vpc_id = var.vpc_id,
    Name = "${var.name}-${var.environment}"
  },
  var.tags,
  )
}
