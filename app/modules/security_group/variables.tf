variable "name" {
  type = string
}

variable "environment" {
  type = string
  default = "<%= "#{Terraspace.env}" %>"
}

variable "tags" {
  type = map(string)
  default = {}
}

variable "vpc_id" {
  description = "The vpc where this SG will exist"
  type = string
}

variable "vpc_cidr" {
  type = string
}

variable "from_port" {
  type = string
}

variable "to_port" {
  type = string
}