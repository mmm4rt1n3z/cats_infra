output "cluster_name" {
  value = local.cluster_name
}

output "cluster_arn" {
  value = aws_ecs_cluster.main.arn
}

output "cluster_id" {
  value = aws_ecs_cluster.main.id
}

output "service_name" {
  value = aws_ecs_service.main.name
}

output "service_id" {
  value = aws_ecs_service.main.id
}

output "task_arn" {
  value = aws_ecs_task_definition.main.arn
}