variable "cluster_name" {
  type = string
  description = "The name of the cluster"
}

variable "service_name" {
  type = string
  description = "The name of the service"
}

variable "container_name" {
  type = string
}

variable "health_check_grace" {
  type = number
  description = "Grace period that ECS will wait to check if the container is healthy"
  default = 600
}

variable "desired_count" {
  type = number
  description = "Desired count of Tasks / Containers running at the same time"
  default = 2
}

variable "launch_type" {
  type = string
  default = "FARGATE" # accepts EC2 / EXTERNAL
}

variable "environment" {
  type = string
  default = "<%= "#{Terraspace.env}" %>"
}

variable "tags" {
  type = map(string)
  default = {}
}

variable "subnets_ids" {
  description = "Set of subnets ids"
  type = set(string)
}

variable "security_groups" {
  description = "Set of security groups"
  type = set(string)
}

variable "container_port" {
  type = number
}

variable "task_cpu" {
  type = number
  description = "The number of CPU units to be assigned to this task definition"
}

variable "task_memory" {
  type = number
}

variable "region" {
  type = string
  description = "AWS region"
}

variable "container_image" {
  type = string
  description = "The docker image that will be used to run the container (it is located in ecr)"
}

variable "alb_target_group_arn" {
  type = string
}

variable "container_policies" {
  type = list(string)
  default = [
    "cloudwatch:GetMetricStatistics",
    "cloudwatch:ListMetrics",
    "cloudwatch:PutMetricData",
    "ecr:BatchCheckLayerAvailability",
    "ecr:BatchGetImage",
    "ecr:GetAuthorizationToken",
    "ecr:GetDownloadUrlForLayer",
    "logs:CreateLogGroup",
    "logs:CreateLogStream",
    "logs:DescribeLogGroups",
    "logs:DescribeLogStreams",
    "logs:PutLogEvents"
  ]
}
