#TODO: This could be DRYER and more modular / avoid the hardcode values (2 days might be)

locals {
  cluster_name = "${var.cluster_name}-${var.environment}"
  service_name = "${var.service_name}-${var.environment}"

  deployment_max_percent = var.desired_count * 100

  # This will ensures at least half of the tasks / containers are gonna be running all the time
  deployment_minimum_health = ((var.desired_count / 2) * 100) / var.desired_count

  container_definition = jsonencode([{
    name        = var.container_name
    image       = var.container_image
    essential   = true
    startTimeout = 240
//    environment = var.container_environment // List of env variables, not needed for now
    portMappings = [{
      protocol      = "tcp"
      containerPort = var.container_port
      hostPort      = var.container_port
    }]
    logConfiguration = {
      logDriver = "awslogs"
      options = {
        awslogs-group         = module.cloudwatch_group.name
        awslogs-stream-prefix = "ecs"
        awslogs-region        = var.region
      }
    }
  }])
}

module "cloudwatch_group" {
  source = "../cloudwatch_log_group"
  name = "/ecs/${var.service_name}-task-${var.environment}"

}

resource "aws_ecs_cluster" "main" {
  name = local.cluster_name

  # this collects data as performance log events using embedded metric format
  setting {
    name  = "containerInsights"
    value = "enabled"
  }

  tags = merge({
    "Name" = local.cluster_name,
    "environment" = var.environment
  },
  var.tags
  )
}

#TODO: these policies are only examples and can / should be more restrictives
module "iam_role" {
  source = "../iam_role"
  name = "${local.service_name}-role"
  policy_to_assume = <<EOF
{
"Version": "2012-10-17",
"Statement": [
  {
    "Action": "sts:AssumeRole",
    "Principal": {
      "Service": "ecs-tasks.amazonaws.com"
    },
    "Effect": "Allow",
    "Sid": ""
  }
]
}
EOF
}

module "iam_policy_ecr" {
  source = "../iam_policy"
  name = "${local.service_name}-policy"
  policy = <<EOF
{
  "Version": "2012-10-17",
  "Statement": [
    {
      "Effect": "Allow",
      "Action": [
        ${join(",", formatlist("\"%s\"", var.container_policies))}
      ],
      "Resource": "*"
    }
 ]
}
EOF
}

module "policy_attachment" {
  depends_on = [module.iam_role, module.iam_policy_ecr]
  source = "../iam_role_policy_attachment"
  policy_arn = module.iam_policy_ecr.arn
  role_name = module.iam_role.name
}

resource "aws_ecs_task_definition" "main" {
  family                   = "${var.service_name}-task-${var.environment}"
  network_mode             = "awsvpc"
  requires_compatibilities = ["FARGATE"]
  cpu                      = var.task_cpu
  memory                   = var.task_memory
  execution_role_arn       = module.iam_role.arn

  container_definitions = local.container_definition

  tags = merge({
    Name = "${var.service_name}-task-${var.environment}",
    environment = var.environment
  },
  var.tags
  )

  lifecycle {
    ignore_changes = [container_definitions]
  }
}

resource "aws_ecs_service" "main" {
  depends_on      = [module.iam_role, aws_ecs_cluster.main, aws_ecs_task_definition.main]
  task_definition = aws_ecs_task_definition.main.arn
  cluster         = aws_ecs_cluster.main.id

  name            = local.service_name
  desired_count   = var.desired_count

  deployment_maximum_percent = local.deployment_max_percent

  deployment_minimum_healthy_percent = local.deployment_minimum_health

  health_check_grace_period_seconds = var.health_check_grace

  launch_type = var.launch_type

  scheduling_strategy = "REPLICA"

  load_balancer {
    target_group_arn = var.alb_target_group_arn
    container_name   = var.container_name
    container_port   = var.container_port
  }

  network_configuration {
    subnets = var.subnets_ids
    security_groups = var.security_groups
    assign_public_ip = true
  }

  # we ignore task_definition changes as the revision changes on deploy
  # of a new version of the application
  # desired_count is ignored as it can change due to autoscaling policy
  lifecycle {
    ignore_changes = [task_definition, desired_count]
  }

  tags = merge({
    Name = local.service_name,
    environment = var.environment
  },
  var.tags
  )
}

resource "aws_appautoscaling_target" "ecs_target" {
  max_capacity       = var.desired_count
  min_capacity       = 1
  resource_id        = "service/${aws_ecs_cluster.main.name}/${aws_ecs_service.main.name}"
  scalable_dimension = "ecs:service:DesiredCount"
  service_namespace  = "ecs"
}

resource "aws_appautoscaling_policy" "ecs_policy_memory" {
  name               = "memory-autoscaling"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageMemoryUtilization"
    }

    target_value       = 80
    scale_in_cooldown  = 300
    scale_out_cooldown = 300
  }
}

resource "aws_appautoscaling_policy" "ecs_policy_cpu" {
  name               = "cpu-autoscaling"
  policy_type        = "TargetTrackingScaling"
  resource_id        = aws_appautoscaling_target.ecs_target.resource_id
  scalable_dimension = aws_appautoscaling_target.ecs_target.scalable_dimension
  service_namespace  = aws_appautoscaling_target.ecs_target.service_namespace

  target_tracking_scaling_policy_configuration {
    predefined_metric_specification {
      predefined_metric_type = "ECSServiceAverageCPUUtilization"
    }

    target_value       = 60
    scale_in_cooldown  = 300
    scale_out_cooldown = 300
  }
}
