output "arn" {
  value = aws_alb_listener.main.arn
}

output "id" {
  value = aws_alb_listener.main.id
}