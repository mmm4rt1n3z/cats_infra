
resource "aws_vpc" "main" {
  cidr_block = var.vpc_cidr
  instance_tenancy = var.instance_tenancy
  enable_dns_hostnames = var.enable_dns_hostnames
  enable_dns_support = var.enable_dns_support
  tags = merge(
  {
    environment = var.environment
  },
  var.vpc_tags,
  )
}

resource "aws_internet_gateway" "gw" {
  vpc_id = aws_vpc.main.id

  tags = merge(
  {
    environment = var.environment
  },
  var.vpc_tags,
  )
}

resource "aws_subnet" "public" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.public_subnet_cidr
  map_public_ip_on_launch = true
  availability_zone = "us-west-1b"

  tags = merge(
  {
    environment = var.environment
  },
  var.public_subnet_tags,
  )
}

resource "aws_subnet" "public1" {
  vpc_id     = aws_vpc.main.id
  cidr_block = var.public_subnet_cidr1
  map_public_ip_on_launch = true
  availability_zone = "us-west-1c"

  tags = merge(
  {
    environment = var.environment
  },
  var.public_subnet_tags,
  )
}

resource "aws_route_table" "main" {
  depends_on = [aws_internet_gateway.gw]
  vpc_id = aws_vpc.main.id

  route {
      cidr_block = "0.0.0.0/0"
      gateway_id = aws_internet_gateway.gw.id
    }

  tags = merge(
  {
    environment = var.environment
  },
  var.vpc_tags,
  )
}

resource "aws_route_table_association" "main" {
  subnet_id      = aws_subnet.public.id
  route_table_id = aws_route_table.main.id
}

resource "aws_route_table_association" "main1" {
  subnet_id      = aws_subnet.public1.id
  route_table_id = aws_route_table.main.id
}