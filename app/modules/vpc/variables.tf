variable "vpc_cidr" {
  type = string
  default = "10.0.0.0/16"
  description = "10.0.0.0/16 allows you to use the IP address that start with “10.0.X.X”"
}

variable "instance_tenancy" {
  description = "A tenancy option for instances launched into the VPC"
  type        = string
  default     = "default"
}

variable "environment" {
  type = string
  default = "<%= "#{Terraspace.env}" %>"
}

variable "vpc_tags" {
  type = map(string)
  default = {}
}

variable "public_subnet_tags" {
  type = map(string)
  default = {}
}

variable "public_subnet_cidr" {
  type = string
  default = "10.0.1.0/24"
  description = "254 public ips available"
}

variable "public_subnet_cidr1" {
  type = string
  default = "10.0.2.0/24"
  description = "254 public ips available"
}

variable "public_ip_on_launch" {
  type = bool
  default = false
  description = "Determines whether the subnet is public or not"
}

variable "enable_dns_hostnames" {
  type        = bool
  default     = false
}

variable "enable_dns_support" {
  type        = bool
  default     = true
}