variable "role_name" {
  type = string
  description = "The role which the specified policy will get attached"
}

variable "policy_arn" {
  type = string
}