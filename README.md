# Terraspace Project

This is a Terraspace project. It contains code to provision Cloud infrastructure built with [Terraform](https://www.terraform.io/) and the [Terraspace Framework](https://terraspace.cloud/).

## Deploy

To deploy all the infrastructure stacks:

    terraspace all up

To deploy individual stacks:

    terraspace up demo # where demo is app/stacks/demo

## Terrafile

To use more modules, add them to the [Terrafile](https://terraspace.cloud/docs/terrafile/).

# Why Terraspace?

Terraspace (TS) is a relatively new framework to work with Terraform (IaC),what is so cool about it?:
_Some Highlights_
- As you may know in Terraform only exist the concept of module and you easily end with a bunch of modules
    which leads you to situations where you have what we call spaghetti code (it sounds delicious but is not).
    Here is where TS makes his entrance, it introduces the concept of stack which basically represents a logic unit
    of infra.
- You can write specs for your infra and actually test some of the stuff locally or in a pipeline
- You can execute ruby code (wait what?), yup which can be handy some times but do not abuse...

# Stacks
- funny_cats
  - The funny_cats stack is in charge of create the main infrastructure for our app

Lets describe what we did:
The idea is to have a fault tolerant infrastructure so we defined an ECS cluster, the service
and the task definition. We set the desired capacity to 2, which means that we want 2 "instances" of our app
running, that means we will have 2 fargate instances and our app will run in each one of those instances inside the docker
container we defined. Then we configured our autoscaling group to keep at least 1 container running all the time and also to scale
in/out based in some basic metrics like cpu and memory usage.

The above causes that only 1 container can be updated at the time which leads you to "rolling updates" meaining no downtime during updates
and in case your app need it it will scale in and out based on the rules defined. Even if a server gets downs, ECS agent installed
in your fargate instance will notify to ECS service and this last one will spin up a new "instance" of your application.

We added a VPC so you app will live inside this VPCs subnets and we sit an application load balancer in front of our app.

Is also attached to this project a brief overview of the architecture...

Also be aware of the default values for some variables like names and accounts_id etc...

## Requirements
- Terraspace and terraform but if you follow terraspace getting started guide you should be fine.
- aws cli configured with at least 1 profile (default one)
- 2 env vars AWS_PROFILE and TS_ENV
- Because we are creating an ecr repository we are also pushing the first docker image 
  (so the ecs tasks can start right after created) which means that is assumed you have **docker** setup
  and you also have a version of the image of the application (funny_cats).
  example: docker build . -t funny-cats:latest

Feel free to contact if have any questions: manueldecub@gmail.com
Thx for reviewing!
